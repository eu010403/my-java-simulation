import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class is for the arena objects
 * @author Mohammed Kasib Rafiq
 */

public class Arena implements Serializable {
    double sX, sY; // Sets size of arena
    private ArrayList<Drone> aDrones; // List of all drone elements in arena
 
    Arena() 
    {
        this(500, 400);	 // Sets the arena size 
    }
  
    /**
     * The arena size of X by Y is defined
     * Initial position of all objects are set
     * @param xaS Size of x axis
     * @param yaS Size of Y axis
     */
    Arena(double xaS, double yaS) 
    {
    	sX = xaS;
        sY = yaS;
        aDrones = new ArrayList<Drone>(); 				
        aDrones.add(new DroneElements(xaS/2, yaS*0.8, 10, 45, 6));	// Drones are added here 
        aDrones.add(new Opponent(xaS/2, yaS/3.5, 35));	// Hit marker opponent is added here
        aDrones.add(new UserEntity(xaS/2, yaS-10, 20));	// User's character is placed here 				
        aDrones.add(new Obstacle(xaS/20, 1*yaS/1.6,25)); 			// Adds obstacles
        aDrones.add(new Obstacle(xaS/8, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(xaS/4, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(xaS/3, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(xaS, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(2*xaS/3, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(2*xaS/8, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(xaS-100, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(xaS-70, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(xaS-40, 1*yaS/1.6, 25));
        aDrones.add(new Obstacle(xaS-180, 1*yaS/6.5,25)); 	// Top 'trap' obstacles
        aDrones.add(new Obstacle(xaS-230, 1*yaS/6.5, 25));
        aDrones.add(new Obstacle(xaS/4, 1*yaS/6.5, 25));
        aDrones.add(new Obstacle(xaS/3, 1*yaS/6.5, 25));
        aDrones.add(new Obstacle(2*xaS/3, 1*yaS/6.5, 25));
        aDrones.add(new Obstacle(xaS-100, 1*yaS/6.5, 25));
    }
    public double getX() {
        return sX; // Returns arena size dictated by X value
    }
    public double getY() {
        return sY; // Returns arena size dictated by Y value
    }

    /**
     * Draws the arena on the canvas
     * @param u
     */
    public void drawArena(CanvasObjects u) {
        for (Drone b : aDrones) b.drawDrone(u);	// Draws all drones
    }
// This checks all drones
    public void checkDrones() {
        for (Drone b : aDrones) b.checkDrone(this);
    }
// This adjusts the drones accordingly
    public void adjustDrones() {
        for (Drone b : aDrones) b.moveDrone();
    }
// Sets the coordinates
    public void userSettings(double x, double y) {
        for (Drone b : aDrones)
            if (b instanceof UserEntity) b.xyS(x, y);
    }

// List describing all drones
    public ArrayList<String> listAll() {
    	ArrayList<String> fact = new ArrayList<String>(); // Array list established
        for (Drone b : aDrones) fact.add(b.toString());	// String data
        return fact;
    }
    
    /**
     * Checks drone angle
     * @param x
     * @param y
     * @param r
     * @param ang
     * @param notID
     * @return the angle when hit
     */
    public double checkDroneA(double x, double y, double r, double ang, int notID) {
        double fact = ang;
        if (x < r || x > sX - r) fact = 180 - fact; // 180� angle is set
        if (y < r || y > sY - r) fact = - fact; 
        for (Drone b : aDrones)
            if (b.getID() != notID && b.hit(x, y, r)) fact = 180*Math.atan2(y-b.getY(), x-b.getX())/Math.PI;
        return fact;	
    }

    /**
     * Hit box check
     * @param target
     * @return result
     */
    public boolean hbCheck(Drone target) {
        boolean fact = false;
        for (Drone b : aDrones)
            if (b instanceof DroneElements && b.collision(target)) fact = true;
        return fact;
    }
    public boolean margin(Drone target) {
    	return !(target.getX() - target.getR() < 0 || target.getY() - target.getR() < 0 || target.getX() + target.getR() > sX || target.getY() + target.getR() > sY);
    }
    public void insertDrone() {
        aDrones.add(new DroneElements(sX/2, sY*0.8, 10, 60, 5)); // Criteria for inserted drones
    }
}