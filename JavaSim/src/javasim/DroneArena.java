package javasim;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

/**
* This class is for implementing the arena as well as configuring the arena size
* The number of drones and their location within the arena can also be defined through user input
* @author Mohammed Kasib Rafiq
*/

public class DroneArena implements Serializable{
	private int length, height;
    ArrayList<Drone> drones;
    private Random rng;

    /**
     * DroneArena class constructor
     * Creates objects with set size parameters
     * @param x length of arena 
     * @param y height of arena 
     */
    public DroneArena(int x, int y){
        this.setSize(x, y);
        this.drones = new ArrayList<Drone>();
        this.rng = new Random();
    }

    public String getSize(){
        return "Length: " + this.getLength() + ", Height: " + this.getHeight();
    }

    public int getLength(){
        return this.length;
    }

    public int getHeight(){
        return this.height;
    }

    /**
     * This method defines the size of the arena
     * @param x length of arena 
     * @param y height of arena 
     */
    public void setSize(int x, int y){
        setLength(x);
        setHeight(y);
    }

    private void setLength(int x){
        this.length = x;
    }

    private void setHeight(int y){
        this.height = y;
    }

    // This method sets the drone to a random location within the arena 
    public void addDrone(){
        int x, y;
        do{
            x = rng.nextInt(this.length);
            y = rng.nextInt(this.height);
        }
        while(this.getDroneAt(x, y));
        this.drones.add(new Drone(x, y, Direction.getRandomDirection()));
    }

    /**
     * This method is used to set the drone to a specified location within the arena
     * @param x position on x axis/length
     * @param y position on y axis/height
     */
    public void addDrone(int x, int y, Direction d){
        if(this.getDroneAt(x, y)){
            System.out.println("SPACE IS ALREADY OCCUPIED");
            return;
        }
        this.drones.add(new Drone(x, y, d));
    }

    /**
     * This method is for accessing information on the arena size, drone UUID and its current position
     * @return Arena size, drone UUID and position
     */
    public String toString(){
        StringBuilder droneLocs = new StringBuilder();
        for (Drone drone : drones){
            droneLocs.append(drone.toString());
        }
        return "Arena size is " + this.getSize() + "\n" + droneLocs;
    }

    /**
     * This checks to see if the drone is at a set location
     * @param x position on x axis/length
     * @param y position on y axis/height
     * @return true if location at x y is occupied otherwise false if empty
     */
    public boolean getDroneAt(int x, int y){
        for (Drone drone : drones) {
            if (drone.isHere(x, y)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This checks if a drone can move to a defined location
     * @param x position on x axis/length
     * @param y position on y axis/height
     * @return true unless drone is out of bounds or if the location contains another drone 
     */
    public boolean canMoveHere(int x, int y){
        return (x <= this.length-1 && x >= 0) && (y <= this.height-1 && y >= 0) && !this.getDroneAt(x, y);
    }

    /**
     * This method is for moving all drones 
     * @param arena containing the drones is made to allow the arena object 
     * to attempt to move every drone when a method is called from another class
     */
    public void moveAllDrones(DroneArena arena){
        for(Drone drone : arena.drones){
            drone.tryToMove(arena, 10);
        }
    }

    /**
     * All drones are marked onto the canvas
     * @param c ConsoleCanvas object is used for displaying the drones
     */
    public void showDrones(ConsoleCanvas c){
        for (Drone drone : drones){
            drone.displayDrone(c);
        }
    }

    public static void main(String[] args){
        DroneArena one = new DroneArena(20,10);
        one.addDrone();
        one.addDrone(10, 10, Direction.EAST);
        System.out.println(one.getDroneAt(10, 10));
        System.out.println(one.toString());
    }
}
