package javasim;

import java.util.Scanner;

import java.io.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
* This is a class used for the user inputs such as adding a drone, getting information on a drone and the arena,
* displaying the drones, creating a new arena, exiting, as well as saving and loading status
* @author Mohammed Kasib Rafiq
*/

public class DroneInterface implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Scanner s;
    private DroneArena myArena;
    ConsoleCanvas myCanvas;

 //This controls the keyboard input in relation to the functionalities defined
public DroneInterface(){
        s = new Scanner(System.in);
        myArena = new DroneArena(20, 6);

        File directory = new File(".\\ArenaSaves");
        if(!directory.exists()) {
        	directory.mkdir();
        }
        int sx = 0;
        int sy = 0;
        char ch = ' ';
        do {
            System.out.print("Enter (A)dd drone, get (I)nformation, (D)isplay drones, (M)ove drones, Anima(T)e drones, (N)ew Arena, (S)ave File, (L)oad file or e(X)it > ");
            ch = s.next().charAt(0);
            s.nextLine();
            switch (ch) {
                case 'd' :
                case 'D' :
                    this.doDisplay();
                    break;
                case 'A' :
                case 'a' :
                    myArena.addDrone();
                    break;
                case 'I' :
                case 'i' :
                    System.out.print(myArena.toString());
                    break;
                case 'M' :
                case 'm' :
                    myArena.moveAllDrones(myArena);
                    this.doDisplay();
                    break;
                case 'N':
                case 'n': 
                	// This uses the user input for the X and Y values to create a new arena
                	// Validation for user input against an invalid input
                	System.out.println("New Arena\n");
                	System.out.println("Enter X size");
                	try {
                		sx = s.nextInt();
                	} catch (Exception err) {
                		
                		System.err.println ("You can only enter numbers.");
                          System.out.println("Enter a new X value");
                		sx = s.nextInt();
                	
                	}
                	System.out.println("Enter Y size");
    				try {
    					sy = s.nextInt();
    				} catch (Exception err) {
    					System.err.println("You can only enter numbers");
    					System.out.println("\nEnter Y ");
    					s.nextInt();
                	} 
    				myArena = new DroneArena(sx, sy); 
    				break;
    				
                case 'S': 
                case 's':
            		int response;
            		File save;
            		JFileChooser jFChooser = new JFileChooser(".\\ArenaSaves");
            	
            		jFChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            		response = jFChooser.showSaveDialog(null);
            		
            		FileOutputStream FOStream;
            		ObjectOutputStream OOStream;
            		if (response == JFileChooser.APPROVE_OPTION) {
            			save = jFChooser.getSelectedFile();
            			try {
            			FOStream = new FileOutputStream(save);
            			OOStream = new ObjectOutputStream (FOStream);
            			OOStream.writeObject(this.myArena);
            			}		
            			catch (IOException err) {
            			
            			System.out.println("Error");
            			err.printStackTrace();
            			}
            		}
                	break;
                	
                case 'T':
                case 't': 
                	for (int i = 0; i < 10; i++) {
                		myArena.moveAllDrones(myArena);
                		doDisplay();
                		try {
                			Thread.sleep(200);
                		} catch (InterruptedException err) {
                			err.printStackTrace();
                	}		
                }
                	break;
                case 'L' :
                case 'l' :
                    int res;
                    File loadFile;

                    JFileChooser fileChooser = new JFileChooser(".\\ArenaSaves");
                    fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                    FileNameExtensionFilter filter = new FileNameExtensionFilter(".save", "save");
                    fileChooser.setFileFilter(filter);
                 
                    res = fileChooser.showOpenDialog(null);
                    if(res == JFileChooser.APPROVE_OPTION){
                        loadFile = fileChooser.getSelectedFile();
                        if(loadFile.isFile()){
                            try {
                                FileInputStream fInStream = new FileInputStream(loadFile);
                                ObjectInputStream oInStream = new ObjectInputStream(fInStream);
                                this.myArena = (DroneArena) oInStream.readObject();
                            }
                            catch (IOException | ClassNotFoundException err) {
                                System.out.println("An error occurred.");
                                err.printStackTrace();
                            }
                        }
                    }
                    break;
                }
        } while (ch != 'X');
        s.close();
    }

//Initialises objects in relation to the size of arena whilst marking the location of the drones prior to displaying the canvas
void doDisplay(){
        int x = myArena.getLength();
        int y = myArena.getHeight();
        this.myCanvas = new ConsoleCanvas(x, y, this.myArena);
        this.myArena.showDrones(this.myCanvas);
        System.out.println(this.myCanvas.toString());
    }

    public static void main(String[] args){
        DroneInterface r = new DroneInterface();
    }
}
