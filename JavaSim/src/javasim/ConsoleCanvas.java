package javasim;

import java.util.Arrays;

 /**
 * This class is designed to display the DroneArena with conditions such as the size of the arena and
 * where the drones are restricted to, alongside a marking for where the drone is currently at
 * @author Mohammed Kasib Rafiq
 */

public class ConsoleCanvas {
    char[][] canvas;
    DroneArena arena;

    /**
     * This is for representing the console canvas objects for the DroneArena
     * @param x contains the length of cArena
     * @param y contains height of cArena
     * @param cArena DroneArena object to display on canvas
     */
    public ConsoleCanvas(int x, int y, DroneArena cArena){
        this.arena = cArena;
        this.canvas = new char[y+2][x+2];
        Arrays.fill(this.canvas[0], '#');
        Arrays.fill(this.canvas[this.canvas.length-1], '#');

        for(int i = 1; i < this.canvas.length-1; i++){
            for(int j = 0; j < this.canvas[i].length; j++){
                if(j == 0 || j == this.canvas[i].length-1){
                    this.canvas[i][j] = '#';
                }
                else{
                    this.canvas[i][j] = ' ';
                }
            }
        }
    }

    /**
     * This inserts a drone onto the canvas and marks the location wherever a drone is present
     * The drone is represented using the character d to show where the drone is within the arena
     * @param x coordinate on the length of droneArena 
     * @param y coordinate on the height of droneArena
     * @param d Direction enum to show direction of drone movement
     * @param c Character to show current location of drone
     */
    public void showIt(int x, int y, Direction d, char c){
        arena.addDrone(x, y, d);
        for(int i = 1; i < this.canvas.length-1; i++){
            for(int j = 1; j < this.canvas[i].length-1; j++){
                if(arena.getDroneAt(j-1,i-1)){
                    this.canvas[i][j] = c;
                }
            }
        }
    }

    /**
     * This is the method for creating the canvas
     * @return canvas with wall boundaries and any present drones
     */
    public String toString(){
        StringBuilder canvasDisplay = new StringBuilder();
        for (int i = 0; i < this.canvas.length; i++){
            canvasDisplay.append(this.canvas[i]);
            canvasDisplay.append('\n');
        }
        return arena.toString() + "\n" + canvasDisplay.toString();
    }

    public static void main(String[] args){
        DroneArena one = new DroneArena(8,3);
        ConsoleCanvas c = new ConsoleCanvas(10, 5, one);
        c.showIt(0, 0, Direction.SOUTH, 'D');
        c.arena.showDrones(c);
        System.out.println(c.toString());
    }
}
