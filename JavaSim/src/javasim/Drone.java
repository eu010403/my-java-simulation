package javasim;

import java.util.UUID;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

 /**
 * This class is designed for implementing the drones within a 2d environment
 * @author Mohammed Kasib Rafiq
 */

public class Drone implements Serializable{
    private int x, y;
    private Direction direction;
    private String id;

    /** This is to move the drone objects around by using a set of specified coordinates along with a random UUID
   * @param x position of drone 
   * @param y position of drone
   */
   public Drone(int x, int y, Direction d){
       this.x = x;
       this.y = y;
       this.direction = d;
       this.id = UUID.randomUUID().toString();
   }

    /** This gets the position of the drone objects
     * @return x and y coordinates 
     */
    public String getPos(){
        return this.getX() + ", " + this.getY();
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

	public Direction getDir() {
		return direction;
	}

    /** This returns the UUID for the drone object
     * @return UUID for drone
     */
    public String getId(){
        return this.id;
    }

    // This method is for setting coordinates for the drone object
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    /** Displays the UUID, coordinates and direction for drone
     * @return UUID, coordinates and direction for drone
     */
    public String toString(){
       return "Drone " + this.getId() + " is at X, Y " + this.getPos() + " and moving " + this.direction + "\n";
    }

    /** This is a check to see if the drone is present at a set location and to return the value when true
     * @param sx checks against value for x coordinate to see whether another drone is present
     * @param sy checks against value for y coordinate to see whether another drone is present
     * @return true if the drone is at the specified location and false otherwise
     */
    public boolean isHere(int sx, int sy){
        return this.x == sx && this.y == sy;
    }

    /**
     * Drones will move around the arena
     * The direction attribute determines how these drones will move
     * The drone will turn if it collides with another drone or if the drone moves out of bounds
     * @param DroneArena is used to store the drone positions
     * @param distance is the specified amount that a drone will attempt to move
     */
    public void tryToMove(DroneArena arena, int distance){
        int i = 0;
        while(i <= distance){
            int changeX = this.x;
            int changeY = this.y;
            int dirIndex = 0;
            switch (this.direction) {
                case NORTH -> changeY -= 1;
                case EAST -> {
                    changeX += 1;
                    dirIndex = 1;
                }
                case SOUTH -> {
                    changeY += 1;
                    dirIndex = 2;
                }
                case WEST -> {
                    changeX -= 1;
                    dirIndex = 3;
                }
            }
            if (arena.canMoveHere(changeX, changeY)) {
                this.x = changeX;
                this.y = changeY;
            } else {
                this.direction = Direction.values()[Direction.getRandomInt(0, 3, dirIndex)];
                System.out.println("Drone " + this.id + " Turning " + this.direction + "\n");
            }
            i++;
            try{
                System.out.println("PAUSE\n");
                TimeUnit.MICROSECONDS.sleep(250);
            }
            catch (InterruptedException exception){
                System.err.format("Exception: %s%n", exception);
            }
        }
    }

   /** Marks the drone using the letter D to display the position
    * @param c ConsoleCanvas is an object that is being used to represent both the arena and drone location
    */
    public void displayDrone(ConsoleCanvas c){
        for(int i = 1; i < c.canvas.length-1; i++){
            for(int j = 1; j < c.canvas[i].length-1; j++){
                if(c.arena.getDroneAt(j-1,i-1)){
                    c.canvas[i][j] = 'D';
                }
            }
        }
    }

    public static void main(String[] args){
        Drone one = new Drone(5,3, Direction.getRandomDirection());
        Drone two = new Drone(7,9, Direction.getRandomDirection());
        System.out.println(one.toString());
        System.out.println(two.toString());
    }
}
