import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.TextAlignment;

/**
 * This class is for elements relating to the canvas
 * @author Mohammed Kasib Rafiq
 */

public class CanvasObjects {
	int sY = 512; // This is to define the canvas size of the Y axis
    int sX = 512; // This is to define the canvas size of the X axis
    
    GraphicsContext gc; // Relating to size and graphics
    public CanvasObjects(GraphicsContext g, int xcs, int ycs) 
    {
    	gc = g;
        sX = xcs;
        sY = ycs;
    }
    public int csX() { // Getter function to retrieves canvas value for size of X  
    	return sX; // Return value
    }
    public int csY() { // Retrieves canvas value for size of Y
    	return sY; // Return value
    }
    
    public void clearCanvas() // Clears the canvas
    {
    	gc.setFill(Color.SILVER); // Sets a silver colour for the canvas
        gc.fillRect(0, 0, sX, sY);
    }
    public void setFill (String colour) {
        gc.setFill(Color.valueOf(colour));
    }
    
/**
 * Draws the rectangle canvas using set measures
 * This provides the fill colour and arrangement
 * @param x value
 * @param y value
 * @param r radiant
 * @param colour 
 */
    public void drawRect(double x, double y, double r, String colour) {
    	setFill(colour); 									
    	gc.fillRect(x-r, y-r, r*2, r*2);	
    }
    
    public void drawCircle(double x, double y, double r, String colour) {
    	setFill(colour); 
        gc.fillArc(x-r, y-r, r*2, r*2, 0, 360, ArcType.ROUND);
    }  

    public void drawText (double x, double y, String s) {
    	gc.setTextAlign(TextAlignment.CENTER);							
        gc.setTextBaseline(VPos.CENTER);							
        gc.setFill(Color.GOLD); // Score text is coloured in gold									          
        gc.fillText(s, x, y); 
    }  // Graphics properties for the user's score
    
    public void showInt (double x, double y, int n) {
    	drawText (x, y, Integer.toString(n));
    }
}