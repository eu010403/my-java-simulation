/**
 * This class is for the drone ball to function
 * @author Mohammed Kasib Rafiq
 */

public class DroneElements extends Drone {
    double Angle, Speed; 
    
    public DroneElements(double intx, double inty, double intr, double angleO, double speedO) {
        super(intx, inty, intr);
        Angle = angleO; // Angle of object
        Speed = speedO; // Speed of object
        colour = "MAGENTA"; // Colour of ball drone
    }
    
    /**
     * Circle drawn 
     * @param u
     */
    public void drawDrone(CanvasObjects u) {
        u.drawCircle(x, y, r, colour);
    }

    // Checks angle of drone
    protected void checkDrone(Arena a) {
        Angle = a.checkDroneA(x, y, r, Angle, UIdrone);
    }

    // Moves the drone accordingly
    protected void moveDrone() {
        double angleR = Angle*Math.PI/180; // Angle of circle radiant
        x += Speed * Math.cos(angleR); // New coordinates of x, y
        y += Speed * Math.sin(angleR); // Speed calculated
    }
    
    protected String getStrType() {
        return "Drone ball"; // GUI label for Drone
    }
}