import java.io.Serializable;

/**
 * This class deals with the Drone 
 * This relates to the ID, size and collision functionality
 * @author Mohammed Kasib Rafiq
 */

public abstract class Drone implements Serializable {
	protected int UIdrone; // Unique ID for drone
    protected double x, y, r; // Coordinates and appearance
    protected String colour; // Colour specification
    static int counter = 0;	// Establishes a difference between each drone

    Drone() {
        this(100, 100, 10);
    }

    /**
     * @param intx
     * @param inty
     * @param intr
     */
    Drone(double intx, double inty, double intr) {
        x = intx;
        y = inty;
        r = intr;
        UIdrone = counter++;	
    }

    public double getX() { return x; } // X value returned
    public double getY() { return y; } // Y value returned
    public double getR() { return r; } // Radius returned

    /**
     * Positioning for drone at new x and new y result
     * @param xN
     * @param yN
     */
    public void xyS(double xN, double yN) {
        x = xN;
        y = yN;
    }

    /**
     * Return the ID of the drone
     * @return
     */
    public int getID() {return UIdrone; }
    
    /**
     * Drone to be drawn as intended and in colour
     * @param u
     */
    public void drawDrone(CanvasObjects u) {
        u.drawRect(x, y, r, colour);
    }
    
    /**
     * @return details
     */
    protected String getStrType() {
        return "score";
    }
    
    public String toString() {
        return getStrType()+" at "+Math.round(x)+", "+Math.round(y); // Return data
    }
    
    protected abstract void moveDrone(); 
    protected abstract void checkDrone(Arena a); 
    
    /**
     * For "hit" contact 
     * @param ox
     * @param oy
     * @param or
     * @return
     */
    public boolean hit(double ox, double oy, double or) {
        return (ox-x)*(ox-x) + (oy-y)*(oy-y) < (or+r)*(or+r);
    }

    /**
     * For drone collisions
     * @param oDrone
     * @return
     */
    public boolean collision (Drone oDrone) {
        return hit(oDrone.getX(), oDrone.getY(), oDrone.getR());
    }
}