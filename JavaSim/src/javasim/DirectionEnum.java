package javasim;

import java.util.Random;

/**
* Class and enum for designating the direction of a drone
* @author Mohammed Kasib Rafiq
*/

public class DirectionEnum{
    public static void main(String[] args){
        System.out.println(Direction.getRandomDirection());
    }
}

enum Direction {
    NORTH,
    EAST,
    SOUTH,
    WEST;

    /**
     * @return Random Direction enum value.
     */
    public static Direction getRandomDirection(){
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }

    /**
     * Uses a range of numbers excluding any specific integers to generate a random number
     * @param start number of the start of range
     * @param end number of the end of range
     * @param exclude any numbers out of range
     * @return random number
     */
    public static int getRandomInt(int start, int end, int... exclude){
        Random random = new Random();
        int randomInt = start + random.nextInt(end - start + 1 - exclude.length);
        for (int ex : exclude) {
            if (randomInt < ex) {
                break;
            }
            randomInt++;
        }
        return randomInt;
    }
}
