/**
 * This class is for the user element
 * @author Mohammed Kasib Rafiq
 */

public class UserEntity extends Drone {

	/**
	 * The user entity is represented by a square
	 * The position of the element is defined by the x and y values
	 * The size of the element is defined by intr
	 * The colouring is set using a hexadecimal value
	 * @param intx as the x axis for user object
	 * @param inty as the y axis for user object
	 * @param intr as the size for user object
	 */
    public UserEntity(double intx, double inty, double intr) 
    {
    	super(intx, inty, intr);
        colour = "#3d0022"; // Assigns a specific purple colour
    }
 
    // The user object is drawn, coloured and positioned using x and y values.
    public void drawDrone(CanvasObjects u) 
    { 
    	u.drawRect(x, y, r, colour);
    }

    // Used for checking drone 
    protected void checkDrone(Arena a) {
    }

    // Used to move drone
    protected void moveDrone() {
    }

    /**
     * @return string for character data
     */
    protected String getStrType() {
    	return "Character";
    }
}