/**
 * This class is for handling the opposing hit marker
 * @author Mohammed Kasib Rafiq
 */

public class Opponent extends Drone {
    private int direction = 1; // Direction of movement
    private double speed = 3; // Speed of hit marker
    private int score;

    /**
     * Score and colour setting is established
     * @param intx
     * @param inty
     * @param intr
     */
    public Opponent(double intx, double inty, double intr) {
    	super(intx, inty, intr);
        score = 0; // Initial score
        colour = "DARKRED"; // Colour of target hit box
    }
    
    protected String getStrType() {
    	return "Score"; // Description of hit marker   
    }

    protected void checkDrone(Arena a) {   
    	if (a.hbCheck(this)) { 
    		score++; // For incrementing the score
    	}
        if (a.margin(this) == false) {       
        	direction = -direction; 
        }
    }
    
    public void drawDrone(CanvasObjects u) {
    	super.drawDrone(u);
    	u.showInt(x, y, score); // Show score on GUI
    }

    protected void moveDrone() {
        x += speed * direction;				
    }
}