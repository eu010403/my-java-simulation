/**
 * This class is for the obstacles in the game
 * It makes reaching the opponent (to score a point) more difficult 
 * @author Mohammed Kasib Rafiq
 */

public class Obstacle extends Drone {
	public Obstacle(double intx, double inty, double intr) {
    	super(intx, inty, intr);
        colour = "PURPLE"; // Applies purple colour
    }
    
    public void drawDrone(CanvasObjects u) {
    	u.drawRect(x, y, r, colour); // Display object
    }

    protected String getStrType() {
    	return "Obstacle"; // GUI label for description
    }
    
    protected void checkDrone(Arena a) { // Check function
        }
   
    protected void moveDrone() { // Move function
    
    }
}