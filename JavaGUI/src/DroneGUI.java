import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

/**
 * This class contains everything for the display of the GUI
 * Menu and sub menus, buttons as well as save and load operations are also defined
 * @author Mohammed Kasib Rafiq
 */

public class DroneGUI extends Application 
{
    private Arena arena;
    private VBox panel;									
	private CanvasObjects u;
    private AnimationTimer timer;

 /**This creates additional information in the form of a pop up message for the user
  * It is enabled when a user selects 'About game' through the 'Help' option in the menu bar
  */
    private void aboutInfo() 
    {
    	Alert alert = new Alert(AlertType.INFORMATION);	
        alert.setTitle("About Game"); //Sets a title for this action							
        alert.setHeaderText(null);
        alert.setContentText("Using your mouse, drag the square and try to score points by directing the ball to the hit marker."); //Displayed text
        alert.showAndWait(); //Enables the user to close the feedback when they are ready
    }
    
    /**
     * When the user drags the mouse they are able to move their character around the arena
     * @param canvas
     */
    void dragMouse (Canvas canvas)     
    {
    	canvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, // The mouse press event should occur
                new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent p) {
                        arena.userSettings(p.getX(), p.getY());
                        drawW();						
                        drawString();
                    }
                });
    }

 // Functions are declared
    MenuBar settingMenu() 
    {
    	MenuBar menuBar = new MenuBar(); // A menu bar is created
        Menu saveFile = new Menu("File"); // File option is created and displayed in the menu bar
        MenuItem mExit = new MenuItem("Exit"); // This is a sub menu for the user to exit the application			
        mExit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent j) {	 // Required action
                timer.stop();	// Timer then stopped
                System.exit(0);	// Program exit
            }
        });
        
        MenuItem mSave = new MenuItem("Save"); // This is a sub menu for a user to save the file
        mSave.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent j) {	
            	save();
            }
        });

        MenuItem mLoad = new MenuItem("Load"); // This is a sub menu for a user to load a saved file			
        mLoad.setOnAction(new EventHandler<ActionEvent>() {
        	public void handle(ActionEvent j) {
        		load();
        	}
        });
        
        saveFile.getItems().addAll(mExit, mSave, mLoad);
        
        Menu myHelper = new Menu("Help");
        MenuItem mAbout = new MenuItem("About Game");
        //Adds the elements to the GUI
        
        mAbout.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
            	aboutInfo(); //Prints the about game information previously defined
            }
        });
        
        myHelper.getItems().addAll(mAbout);	
        menuBar.getMenus().addAll(saveFile, myHelper);	
        return menuBar;	
    }

// Button setup
    private HBox setButtons() { //This is used for setting up the required buttons
        
    	Button btnPlay = new Button("Play"); //Button for starting the game
        btnPlay.setOnAction(click -> timer.start()); //Starts when action is initiated

        Button btnPause = new Button("Pause"); //Button for pausing the game 
        btnPause.setOnAction(click -> timer.stop()); //Pauses the game when the user makes the action
        
        Button btnaddDrone = new Button("Add Drone"); //Button for adding additional drones
        btnaddDrone.setOnAction(click -> {arena.insertDrone(); drawW();}); //Created when the user makes the action

        return new HBox(btnPlay, btnPause, btnaddDrone); //This now adds the buttons to the GUI within a HBox
    }

 // Sets the point score positioning
    public void showScore (double x, double y, int score) {
    	u.drawText(x, y, Integer.toString(score));
    }
   
 // Draws the arena elements to the GUI
    public void drawW () {
    	u.clearCanvas();						
        arena.drawArena(u);
    }

 // Uses the right side panel to display additional movement information
    public void drawString() {
    	panel.getChildren().clear();				
        ArrayList<String> allDs = arena.listAll();
        for (String s : allDs) {
            Label l = new Label(s); 
            panel.getChildren().add(l);	
        }
    }

    public void start(Stage primaryStage) throws Exception {
        
        primaryStage.setTitle("Drone Game"); // Sets the main title to 'Drone Game'
        BorderPane p = new BorderPane();
        p.setPadding(new Insets(25, 20, 15, 20));
        p.setTop(settingMenu()); // Sets the menu to the top of the screen
        Group root = new Group(); // Creates the canvas area
        Canvas canvas = new Canvas( 400, 500 );
        root.getChildren().add( canvas );
        p.setLeft(root); //Sets the play area to the left side
        u = new CanvasObjects(canvas.getGraphicsContext2D(), 400, 500);
        dragMouse(canvas);	// For use of the mouse
        arena = new Arena(400, 500);
        drawW();

        timer = new AnimationTimer() {	
        	public void handle(long currentNanoTime) {		
            	arena.checkDrones(); // Checks position drones
                arena.adjustDrones(); // Adjusts the drones
                drawW(); // Draws the world 
                drawString(); // Draws the string information				
            }
        };

        panel = new VBox();	// Area for string data of movement
        panel.setAlignment(Pos.TOP_RIGHT); // Sets alignment position
        panel.setPadding(new Insets(5, 75, 75, 5));	//Sets padding
        p.setRight(panel); // Sets the panel to the right of the interface 								
        p.setBottom(setButtons()); // Sets the buttons and places them in the bottom

    // Scene properties
        Scene scene = new Scene(p, 700, 600); 					
        p.prefHeightProperty().bind(scene.heightProperty());
        p.prefWidthProperty().bind(scene.widthProperty());
        primaryStage.setScene(scene);
        primaryStage.show(); // Displays on screen
    }
   
 // Load function for saved text files
    public void load() {
    	String url = "SaveFile.txt";
    	try {
    		ObjectInputStream loader = new ObjectInputStream(new FileInputStream(url)); // Select file process
    		arena = (Arena) loader.readObject(); // Read file process
    		loader.close();  // Loader is closed
    		drawW(); // World is now drawn from save file
    	}
    	catch(Exception e) {    	
    		System.out.println(e); // Prints an exception if a file cannot be established
    	}
    }

 // Save function for any games that are in progress
    public void save() { 
    	String url = "SaveFile.txt"; // Default save name
    	try {
    		ObjectOutputStream saver = new ObjectOutputStream(new FileOutputStream(url)); // Save location process
    		saver.writeObject(arena); // File written to directory
    		saver.close(); // Save window closed
    	} 
    	catch(Exception e) {
    		System.out.println(e); // Prints an exception if applicable
    	}
    }

// GUI application launch
    public static void main(String[] args) {
    	Application.launch();
    }
}